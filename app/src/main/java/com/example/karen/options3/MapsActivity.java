package com.example.karen.options3;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private PolylineOptions polylineOptions;
    private ArrayList<LatLng> arrayPoints;
    private LatLng newLatLng;
    private Context context = this;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Utils.checkPermissions(this);
        arrayPoints = new ArrayList<LatLng>();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            //TODO: fix bug. when is gps enable and location permission enable service will be started
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.clear:
                mMap.clear();
                count = 0;
                arrayPoints.clear();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (count <= 1) {
                    MarkerOptions marker = new MarkerOptions();
                    marker.position(latLng);
                    mMap.addMarker(marker);
                    polylineOptions = new PolylineOptions();
                    polylineOptions.color(Color.RED);
                    polylineOptions.width(5);
                    arrayPoints.add(latLng);
                    polylineOptions.addAll(arrayPoints);
                    mMap.addPolyline(polylineOptions);
                    if (count == 1) {
                        float distance = calcBetweenDistance();
                        addText(context, mMap, newLatLng, distance, 20, 15);
                    }
                    count++;
                }
            }
        });
    }

    private float calcBetweenDistance() {
        Location start = null;
        Location end = null;
        double startLatitude = 0;
        double startLongitude = 0;
        double endLatitude = 0;
        double endLongitude = 0;
        for (int i = 0; i < arrayPoints.size(); i++) {
            LatLng latLng = arrayPoints.get(i);
            double latitude = latLng.latitude;
            double longitude = latLng.longitude;
            if (i == 0) {
                start = new Location("point A");
                start.setLatitude(latitude);
                start.setLongitude(longitude);
                startLatitude = latitude;
                startLongitude = longitude;
            } else {
                end = new Location("point B");
                end.setLatitude(latitude);
                end.setLongitude(longitude);
                endLatitude = latitude;
                endLongitude = longitude;
            }
        }
        newLatLng = calcBetweenCoordinate(startLatitude, startLongitude, endLatitude, endLongitude);
        float distance = start.distanceTo(end);
        return distance;
    }

    private Marker addText(final Context context, final GoogleMap map,
                           final LatLng location, final float length, final int padding,
                           final int fontSize) {
        Marker marker = null;
        String text = String.valueOf(length) + "m";
        if (context == null || map == null || location == null || text == null
                || fontSize <= 0) {
            return marker;
        }

        final TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextSize(fontSize);
        final Paint paintText = textView.getPaint();
        final Rect boundsText = new Rect();
        paintText.getTextBounds(text, 0, textView.length(), boundsText);
        paintText.setTextAlign(Paint.Align.CENTER);
        final Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        final Bitmap bmpText = Bitmap.createBitmap(boundsText.width() + 2
                * padding, boundsText.height() + 2 * padding, conf);

        final Canvas canvasText = new Canvas(bmpText);
        paintText.setColor(Color.BLUE);
        canvasText.drawText(text, canvasText.getWidth() / 2,
                canvasText.getHeight() - padding - boundsText.bottom, paintText);
        final MarkerOptions markerOptions = new MarkerOptions()
                .position(location)
                .icon(BitmapDescriptorFactory.fromBitmap(bmpText))
                .anchor(0.5f, 1);
        marker = map.addMarker(markerOptions);

        return marker;
    }

    private LatLng calcBetweenCoordinate(double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
        double betweenLatitude = ((endLatitude - startLatitude) / 2) + startLatitude;
        double betweenLongitude = ((endLongitude - startLongitude) / 2) + startLongitude;
        LatLng betweenlatLng = new LatLng(betweenLatitude, betweenLongitude);
        return betweenlatLng;
    }
}
